<html lang="es">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <style>
    #SO
    {
      width:120px;
      float:right;
      margin-right:40px;
    }
    th,td
    {
      font-size: 16px;
    }
  </style>
  <script>
    function chargeRespaldos(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="respaldosPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir respaldos
          theRespaldosPlace = document.createElement("td");
          theRespaldos = document.createElement("input");
          theRespaldos.name="respaldos"+theData[0];
          theRespaldos.value=theData[1];
          theRespaldos.type="text";
          theRespaldos.required=true;
          theRespaldos.autocomplete="off";
          theRespaldosPlace.appendChild(theRespaldos);
          theRespaldos.size=34;
        //  añadir Response1
          theResponse1Place = document.createElement("td");
          theResponse1 = document.createElement("input");
          if(theData[2]==1)
            theResponse1.checked=1;
          theResponse1.value=1;
          theResponse1.name="response1"+theData[0];
          theResponse1.type="radio";
          theResponse1Place.appendChild(theResponse1);
          theResponse1 = document.createElement("input");
          theResponse1.value=0;
          theResponse1.name="response1"+theData[0];
          theResponse1.type="radio";
          if(theData[2]==0)
            theResponse1.checked=1;
          theResponse1Place.appendChild(theResponse1);
        //  añadir comentarios
          theComentariosPlace = document.createElement("td");
          theComentarios = document.createElement("input");
          theComentarios.name="comentarios"+theData[0];
          theComentarios.value=theData[3];
          theComentarios.type="text";
          theComentarios.autocomplete="off";
          theComentariosPlace.appendChild(theComentarios);
          theComentarios.size=60;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfRespaldos"+theData[0];
          newTabla.id = "childOfRespaldos"+theData[0];
          newTabla.appendChild(theRespaldosPlace);
          newTabla.appendChild(theResponse1Place);
          newTabla.appendChild(theComentariosPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfRespaldos").value=document.getElementById("childOfRespaldos").value++;
    }
    function chargeUsuarios(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="usuariosPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir dbuName
          theDBUNamePlace = document.createElement("td");
          theDBUName = document.createElement("input");
          theDBUName.name="dbuName"+theData[0];
          theDBUName.value=theData[1];
          theDBUName.required=true;
          theDBUName.type="text";
          theDBUName.autocomplete="off";
          theDBUNamePlace.appendChild(theDBUName);
          theDBUName.size=20;
        //  añadir dbuRol
          theDBURolPlace = document.createElement("td");
          theDBURol = document.createElement("input");
          theDBURol.name="dbuRol"+theData[0];
          theDBURol.value=theData[2];
          theDBURol.type="text";
          theDBURol.autocomplete="off";
          theDBURol.required=true;
          theDBURolPlace.appendChild(theDBURol);
          theDBURol.size=34;
        //  añadir Privilegios
          theDBUPrivilegiosPlace = document.createElement("td");
          theDBUPrivilegios = document.createElement("input");
          theDBUPrivilegios.name="dbuPrivilegios"+theData[0];
          theDBUPrivilegios.value=theData[3];
          theDBUPrivilegios.type="text";
          theDBUPrivilegios.autocomplete="off";
          theDBUPrivilegiosPlace.appendChild(theDBUPrivilegios);
          theDBUPrivilegios.size=46;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfUsuarios"+theData[0];
          newTabla.id = "childOfUsuarios"+theData[0];
          newTabla.appendChild(theDBUNamePlace);
          newTabla.appendChild(theDBURolPlace);
          newTabla.appendChild(theDBUPrivilegiosPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfUsuarios").value=document.getElementById("childOfUsuarios").value++;
    }
    function chargeEspecificaciones(theInfo)
    {
      //  recuperar informacion de equipo
        workplace1="specPlaceholder";
        workplace1=document.getElementById(workplace1);
      // inteligencia
        //  create nueva tabla de discos
          theData = theInfo.split(",");
          newTabla = document.createElement("tr");
        //  añadir especificacion
          theSpecPlace = document.createElement("td");
          theSpec = document.createElement("input");
          theSpec.name="especificacion"+theData[0];
          theSpec.value=theData[1];
          theSpec.type="text";
          theSpec.autocomplete="off";
          theSpecPlace.appendChild(theSpec);
          theSpec.size=25;
        //  añadir valor
          theValuePlace = document.createElement("td");
          theValue = document.createElement("input");
          theValue.name="valor"+theData[0];
          theValue.value=theData[2];
          theValue.type="text";
          theValue.autocomplete="off";
          theValuePlace.appendChild(theValue);
          theValue.size=15;
        //  añadir valor Default
          theValDefPlace = document.createElement("td");
          theValDef = document.createElement("input");
          theValDef.name="valorDef"+theData[0];
          theValDef.value=theData[3];
          theValDef.type="text";
          theValDef.autocomplete="off";
          theValDefPlace.appendChild(theValDef);
          theValDef.size=60;
        //  agregar datos predefinidos a nueva tabla
          newTabla.name = "childOfSpec"+theData[0];
          newTabla.id = "childOfSpec"+theData[0];
          newTabla.appendChild(theSpecPlace);
          newTabla.appendChild(theValuePlace);
          newTabla.appendChild(theValDefPlace);
          workplace1.parentNode.insertBefore(newTabla,workplace1);
      document.getElementById("childOfSpec").value=document.getElementById("childOfSpec").value++;
    }
  </script>
  <?php
    echo "<title>Database de maquina </title>";
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
</head>
<body>
  <?php
    $sql = "select puerto,DBSolicitada,DBEntregado,especificaciones,respaldos,usuarios from especificacionesDB where folioNumber='".$_POST['machine']."'";
    $r=mysqli_query($conn,$sql);
    if(mysqli_affected_rows($conn)==1)
      $firstDBInfo = mysqli_fetch_array($r);
  ?>
  <div class="container">
    <form method='POST' action="processIntDatabase.php" >
      <br>
      <!-- Botones principales -->
        <div align="center">
          <input type="submit" value="Modificar especificaciones">
        </div>
        <br>
      <input type="hidden" id="childOfRespaldos" name="childOfRespaldos" value="<?php if($firstDBInfo['respaldos']){echo $firstDBInfo['respaldos'];} else {echo "0";};?>">
      <input type="hidden" id="childOfUsuarios" name="childOfUsuarios" value="<?php if($firstDBInfo['usuarios']){echo $firstDBInfo['usuarios'];} else {echo "0";};?>">
      <input type="hidden" id="childOfSpec" name="childOfSpec" value="<?php if($firstDBInfo['especificaciones']){echo $firstDBInfo['especificaciones'];} else {echo "0";};?>">
      <input type="hidden" name="machine" id="machine" value="<?php echo $_POST['machine'];?>" >
      <input type="hidden" name="folio" id="folio" value="<?php echo $_POST["folio"];?>" >
      <table width="100%">
        <!-- Renglon   1   -->
          <tr>
            <!-- dBase -->
              <td width="25%">
                Database solicitada: <input type="text" name="dBaseSolicitada" id="dBaseSolicitada" readonly="readonly" value="<?php echo $firstDBInfo['DBSolicitada']; ?>">
                <br>
                Database entregada: <input type="text" name="dBaseEntregada" id="dBaseEntregada" onkeypress="return rev(event)" value="<?php echo $firstDBInfo['DBEntregado']; ?>" autocomplete="off">
              </td>
            <!-- puerto -->
              <td width="14%">
                Puerto: <input type="number" id="puerto" required name="puerto" style="max-width:60px;" min="1" step="1" max="100000" value="<?php echo $firstDBInfo['puerto']; ?>" autocomplete="off">
              </td>
            <!-- respaldos -->
              <td width="13%">
                Respaldos: <select id="respaldos" name="respaldos" onchange="giveRespaldos(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {
                      echo "<option value=\"".$o."\"";
                      if($firstDBInfo['respaldos']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- especificaciones -->
              <td width="18%">
                Especificaciones: <select id="especificaciones" name="especificaciones" onchange="giveSpc(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {
                      echo "<option value=\"".$o."\"";
                      if($firstDBInfo['especificaciones']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- usuarios -->
              <td width="13%">
                Usuarios: <select id="usuarios" name="usuarios" onchange="giveUsuarios(this)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                    {
                      echo "<option value=\"".$o."\"";
                      if($firstDBInfo['usuarios']==$o)
                        echo " selected ";
                      echo ">".$o."</option>";
                    }
                  ?>
                </select>
              </td>
            <!-- reporte -->
              <td width="17%">
                <a href="reporteBD.php?folio=<?php echo $_POST['machine']; ?>">Generar Documento</a>
              </td>
          </tr>
      </table>
      Respaldos:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="30%">Respaldos</th>
            <th width="15%">Si/no</th>
            <th width="55%">Comentarios</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="respaldosPlaceholder" ></tr>
      </table>
      <?php
        if($firstDBInfo['respaldos']>0)
        {
          try
          {
            $sql = "select respaldos,response1,comentarios from DBRespaldos where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['respaldos'].",".$rowOfUser['response1'].",".$rowOfUser['comentarios'];
              echo '<script type="text/javascript">chargeRespaldos("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
      Especificaciones:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="25%">Especificacion</th>
            <th width="15%">valor</th>
            <th width="60%">Valor default</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="specPlaceholder" ></tr>
      </table>
      <?php
        if($firstDBInfo['especificaciones']>0)
        {
          try
          {
            $sql = "select especificacion,valor,valorDef from DBEspecificaciones where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfSpecs = mysqli_fetch_array($r))
            {
              $rowSpecs=$a.",".$rowOfSpecs['especificacion'].",".$rowOfSpecs['valor'].",".$rowOfSpecs['valorDef'];
              echo '<script type="text/javascript">chargeEspecificaciones("'.$rowSpecs.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
      Usuarios:
      <table width="100%">
        <!-- Renglon   titulos   -->
          <tr>
            <th width="20%">Usuario</th>
            <th width="30%">Rol (aplicativo,<br>Consulta, reporteo, etc</th>
            <th width="50%">Privilegios</th>
          </tr>
        <!-- Placeholder2 -->
          <tr id="usuariosPlaceholder" ></tr>
      </table>
      <?php
        if($firstDBInfo['usuarios']>0)
        {
          try
          {
            $sql = "select dbuName,dbuRol,dbuPrivilegios from DBUsuarios where folioNumber='".$_POST['machine']."'";
            $r=mysqli_query($conn,$sql);
            $a=0;
            while($rowOfUser = mysqli_fetch_array($r))
            {
              $rowUsr=$a.",".$rowOfUser['dbuName'].",".$rowOfUser['dbuRol'].",".$rowOfUser['dbuPrivilegios'];
              echo '<script type="text/javascript">chargeUsuarios("'.$rowUsr.'");</script>';
              $a++;
            }
          }
          catch(Error $e)
          {
            echo '<script type="text/javascript">alert("Error de conexion con base de datos '.$e.'");<script>';
          }
        }
      ?>
    </form>
  </div>
</body>
<script>
  function rev(event)
  {
    var k = (event.which) ? event.which : event.keyCode;
    if ((k > 47 && k < 58)||(k > 64 && k < 91)||(k > 96 && k < 123)||(k == 160)||(k == 95)||(k == 45) ||(k == 44) ||(k ==32) )
      return true;
    else
      return false;
  }
  function giveRespaldos(select)
  {
    //  recuperar informacion de equipo
      workplace1="respaldosPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfRespaldos").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfRespaldos"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos
            newTabla = document.createElement("tr");
          //  añadir respaldos
            theRespaldosPlace = document.createElement("td");
            theRespaldos = document.createElement("input");
            theRespaldos.name="respaldos"+creatorOfNodes;
            theRespaldos.value="Respaldos "+(creatorOfNodes+1);
            theRespaldos.type="text";
            theRespaldos.required=true;
            theRespaldosPlace.appendChild(theRespaldos);
            theRespaldos.size=34;
          //  añadir Response1
            theResponse1Place = document.createElement("td");
            theResponse1 = document.createElement("input");
            theResponse1.value=1;
            theResponse1.name="response1"+creatorOfNodes;
            theResponse1.type="radio";
            theResponse1Place.appendChild(theResponse1);
            theResponse1 = document.createElement("input");
            theResponse1.value=0;
            theResponse1.name="response1"+creatorOfNodes;
            theResponse1.type="radio";
            theResponse1Place.appendChild(theResponse1);
          //  añadir comentarios
            theComentariosPlace = document.createElement("td");
            theComentarios = document.createElement("input");
            theComentarios.name="comentarios"+creatorOfNodes;
            theComentarios.value="Comentarios "+(creatorOfNodes+1);
            theComentarios.type="text";
            theComentariosPlace.appendChild(theComentarios);
            theComentarios.size=60;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfRespaldos"+creatorOfNodes;
            newTabla.id = "childOfRespaldos"+creatorOfNodes;
            newTabla.appendChild(theRespaldosPlace);
            newTabla.appendChild(theResponse1Place);
            newTabla.appendChild(theComentariosPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfRespaldos").value=select.value;
  }
  function giveSpc(select)
  {
    //  recuperar informacion de equipo
      workplace1="specPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfSpec").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfSpec"+mimic));
        }
      }
      else
      {
        //  Crear especificaciones extra
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla
            newTabla = document.createElement("tr");
          //  añadir Especificacion
            theSpecPlace = document.createElement("td");
            theSpec = document.createElement("input");
            theSpec.name="especificacion"+creatorOfNodes;
            theSpec.value="Especificacion "+(creatorOfNodes+1);
            theSpec.type="text";
            theSpec.required=true;
            theSpecPlace.appendChild(theSpec);
            theSpec.size=25;
          //  añadir valor
            theValuePlace = document.createElement("td");
            theValu1 = document.createElement("input");
            theValu1.name="valor"+creatorOfNodes;
            theValu1.value="Valor "+(creatorOfNodes+1);
            theValu1.type="text";
            theValu1.required=true;
            theValuePlace.appendChild(theValu1);
            theValu1.size=15;
          //  añadir comentarios
            theValDefPlace = document.createElement("td");
            theValDef = document.createElement("input");
            theValDef.name="valorDef"+creatorOfNodes;
            theValDef.value="Valor default "+(creatorOfNodes+1);
            theValDef.type="text";
            theValDefPlace.appendChild(theValDef);
            theValDef.size=60;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfSpec"+creatorOfNodes;
            newTabla.id = "childOfSpec"+creatorOfNodes;
            newTabla.appendChild(theSpecPlace);
            newTabla.appendChild(theValuePlace);
            newTabla.appendChild(theValDefPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfSpec").value=select.value;
  }
  function giveUsuarios(select)
  {
    //  recuperar informacion de equipo
      workplace1="usuariosPlaceholder";
      workplace1=document.getElementById(workplace1);
      mimic=document.getElementById("childOfUsuarios").value;
    // inteligencia
      if(select.value<=mimic)
      {
        while(select.value!=mimic)
        {
          mimic--;
          workplace1.parentNode.removeChild(document.getElementById("childOfUsuarios"+mimic));
        }
      }
      else
      {
        //  Crear usuarios extra
        for(var creatorOfNodes=parseInt(mimic);creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos
            newTabla = document.createElement("tr");
          //  añadir dbuName
            theDBUNamePlace = document.createElement("td");
            theDBUName = document.createElement("input");
            theDBUName.name="dbuName"+creatorOfNodes;
            theDBUName.value="Nombre "+(creatorOfNodes+1);
            theDBUName.required=true;
            theDBUName.type="text";
            theDBUNamePlace.appendChild(theDBUName);
            theDBUName.size=20;
          //  añadir dbuRol
            theDBURolPlace = document.createElement("td");
            theDBURol = document.createElement("input");
            theDBURol.name="dbuRol"+creatorOfNodes;
            theDBURol.value="Rol "+(creatorOfNodes+1);
            theDBURol.required=true;
            theDBURol.type="text";
            theDBURolPlace.appendChild(theDBURol);
            theDBURol.size=34;
          //  añadir Privilegios
            theDBUPrivilegiosPlace = document.createElement("td");
            theDBUPrivilegios = document.createElement("input");
            theDBUPrivilegios.name="dbuPrivilegios"+creatorOfNodes;
            theDBUPrivilegios.value="Privilegios "+(creatorOfNodes+1);
            theDBUPrivilegios.type="text";
            theDBUPrivilegiosPlace.appendChild(theDBUPrivilegios);
            theDBUPrivilegios.size=46;
          //  agregar datos predefinidos a nueva tabla
            newTabla.name = "childOfUsuarios"+creatorOfNodes;
            newTabla.id = "childOfUsuarios"+creatorOfNodes;
            newTabla.appendChild(theDBUNamePlace);
            newTabla.appendChild(theDBURolPlace);
            newTabla.appendChild(theDBUPrivilegiosPlace);
            workplace1.parentNode.insertBefore(newTabla,workplace1);
        }
      }
    document.getElementById("childOfUsuarios").value=select.value;
  }
</script>
</html>