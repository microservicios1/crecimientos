<html>
<head> 
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Maquinas registradas </title>
  <style> 
    body 
    { 
      background-size: 160px  75px; 
      background-position: 94% 20px; 
    } 
    .evilbtn 
    { 
      font-size: 10px; 
      height: 40px; 
    } 
  </style>
</head>
<body>
  <div class="container" align="center">
    <br><br><br><br>
    <?php
      include 'dbc.php';
      $conn = mysqli_connect($host, $user, $pass, $db);
      $noError=1;
      if(! $conn )
        echo "<p>Conexion sql fallida!'</p>";
      else
      {
        // limpiar maquinas existentes con folio 
          mysqli_begin_transaction($conn, MYSQLI_TRANS_START_READ_WRITE);
          $sql="delete from growMaquinasTP1 where folio='".$_POST['folio']."'";
          mysqli_query($conn,$sql);
        $expectedData =array('interId','aplicacion','ambiente','CPU','RAM','nombre','infraestructuraDef','estatus','detalleEstatus','TDOYM','tipo');
        $cantidadMaquinas=$_POST['totalMaquinas'];
        for($i=0;$i<$cantidadMaquinas;$i++)
        {
          for($j=0;$j<sizeof($expectedData);$j++)  
            $numeredData[$j]=$expectedData[$j].$i;
          if($noError==1)
          {
            $sql="insert into growMaquinasTP1 values ('".$_POST['folio']."','".$_POST[$numeredData[0]]."','".$_POST[$numeredData[1]]."','".$_POST[$numeredData[2]]."',";
            if($_POST[$numeredData[3]]!="")
              $sql .= $_POST[$numeredData[3]].",";
            else
              $sql .= "0,";
            if($_POST[$numeredData[4]]!="")
              $sql .= $_POST[$numeredData[4]].",'";
            else
              $sql .= "0,'";
            $sql .= $_POST[$numeredData[5]]."','".$_POST[$numeredData[6]]."','".$_POST[$numeredData[7]]."','".$_POST[$numeredData[8]]."',";
            if($_POST[$numeredData[9]]!="")
              $sql .= $_POST[$numeredData[9]].")";
            else
              $sql .= "0)";
            mysqli_query($conn,$sql);
            $r=mysqli_affected_rows($conn);
            if($r<1)
            {
              $noError=0;
              echo "<p>Conexion con BD fallida</p>";
            }
            else 
            {
              $sql="delete from growDiscos where interId='".$_POST['folio'].($i+1)."'";
              mysqli_query($conn,$sql);
              giveMem($conn,$i);
              if($_POST[$numeredData[10]]=="Cluster")
                giveShMe($conn,$i);
            }
          }
        }
        if($noError==0)
        {
          mysqli_rollback($conn);
          echo "<br><p>base de datos no alcanzada, <span style=\"font-color:red;font-size:18px\">registro fallido</span><br></p>";
        }
        else
        {
          mysqli_commit($conn);
          echo "<br><p>Cambios/Crecimientos solicitados<br></p>";
        }
      }
      function giveMem($conn,$machine)
      {
        $cantidadDiscos='staticDisk'.$machine;
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]='name'.$k.'e'.$machine;
          $datosDisco[1]='tam'.$k.'e'.$machine;
          $datosDisco[2]='des'.$k.'e'.$machine;
          $h=$machine+1;
          $sql="insert into growDiscos values ('".$_POST['folio'].$h."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "Disk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Estatico','".$_POST[$datosDisco[2]]."')"; 
          echo $sql."<br>";
          mysqli_query($conn,$sql);
        }
      }
      function giveShMe($conn,$machine)
      {
        $cantidadDiscos='sharedDisk'.$machine;
        for($k=0;$k<$_POST[$cantidadDiscos];$k++)
        {
          $datosDisco[0]='namo'.$k.'e'.$machine;
          $datosDisco[1]='tamo'.$k.'e'.$machine;
          $datosDisco[2]='deso'.$k.'e'.$machine;
          $h=$machine+1;
          $sql="insert into growDiscos values ('".$_POST['folio'].$h."','";
          if($_POST[$datosDisco[0]]!="")
            $sql .= $_POST[$datosDisco[0]];
          else 
            $sql .= "SharedDisk".($k+1);
          $sql .= "',".$_POST[$datosDisco[1]].",'Compartido','".$_POST[$datosDisco[2]]."')"; 
          echo $sql."<br>";
          mysqli_query($conn,$sql);
        }
      }
    ?>
    <button onclick=window.close();>Continuar</button>
    <p>  </p><br>
    <?php mysqli_close($conn); ?>
    <!--<button type="button" class="evilbtn">TD Automatización <br>Servicios Infraestructura</button>-->
    <p>  </p><br><br>
  </div>
</body>
</html>