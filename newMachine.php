<html lang="es"> 
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <?php
      if($_POST['action']==2||$_POST['action']==4)
        echo "<title>Modificacion de  VM</title>";
      if($_POST['action']==1||$_POST['action']==3)
        echo "<title>Registro de VM</title>";
      include 'dbc.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
      $sql="select MAX(arreglo) from maquinas where folio='".$_POST['folio']."'";
      $rr=mysqli_query($conn,$sql);
      $arregloBG=mysqli_fetch_array($rr);
      $sql="select count(interId) from maquinas where folio='".$_POST['folio']."'";
      $rr=mysqli_query($conn,$sql);
      $actMaquinas=mysqli_fetch_array($rr);
    ?>
    <style>
      table
      {
        border: 1px solid #000000;
        text-align: center;
        border-collapse: collapse;
      }
      td,th 
      {
        border: 1px solid #000000;
        padding: 3px 2px;
      }
      .containerOfRod
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 14px;
        border:10px groove #616161;
        border-radius: 10px;
      }
    </style>
  </head>
  <script>
    function cleanDisk(diskName)
    {
      diskName=document.getElementById(diskName);
      while (diskName.firstChild)
        diskName.removeChild(diskName.firstChild);
    }
    function addStatic(diskInfo)
    {
      //  recibir los datos
        var diskData = diskInfo.split(",");
      //  recuperar en que disco se trabajara
        workInThisDisk="staticPlaceholder"+diskData[1];
        workInThisDisk=document.getElementById(workInThisDisk);
      //  create nueva tabla de discos
        newTabla = document.createElement("tr");
        newTabla.style="display:inline-block;";
      //  añadir nombre de disco
        theDiskName = document.createElement("input");
        theDiskName.name="name"+diskData[0]+"e"+diskData[1];
        theDiskName.value=diskData[2];
        theDiskName.type="text";
        theDiskName.size=6;
      //  añadir tamaño de disco definiendo primeros 2 con minimo
        theDiskSize= document.createElement("input");
        theDiskSize.type="number";
        theDiskSize.required=true;
        theDiskSize.style="max-width:50px;";
        theDiskSize.name="tam"+diskData[0]+"e"+diskData[1];
        theDiskSize.max=2048;
        theDiskSize.min=1;
        theDiskSize.value=diskData[3];
        theDiskSize.step=1;
      //  añadir descripcion de disco
        theDiskDescription = document.createElement("input");
        theDiskDescription.name="des"+diskData[0]+"e"+diskData[1];
        theDiskDescription.value=diskData[4];
        theDiskDescription.type="text";
        theDiskDescription.size=6;
      /*  añadir cantidad de discos identicos
        TheDiskQuantity= document.createElement("input");
        TheDiskQuantity.type="number";
        TheDiskQuantity.required=true;
        TheDiskQuantity.style="max-width:35px;";
        TheDiskQuantity.name="QU"+diskData[0]+"e"+diskData[1];
        TheDiskQuantity.max=20;
        TheDiskQuantity.min=1;
        TheDiskQuantity.step=1;
        TheDiskQuantity.value=diskData[5];*/
      //  agregar datos predefinidos a nueva tabla 
        newTabla.appendChild(theDiskName);
        newTabla.appendChild(theDiskSize);
        newTabla.appendChild(theDiskDescription);
        /*newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.appendChild(newTabla);
    }
    function addShared(diskInfo)
    {
      //  recibir los datos
        var diskData = diskInfo.split(",");
      //  recuperar en que disco se trabajara
        workInThisDisk="sharedPlaceholder"+diskData[1];
        workInThisDisk=document.getElementById(workInThisDisk);
      //  create nueva tabla de discos
        newTabla = document.createElement("tr");
        newTabla.style="display:inline-block;";
      //  añadir nombre de disco
        theDiskName = document.createElement("input");
        theDiskName.name="namo"+diskData[0]+"e"+diskData[1];
        theDiskName.value=diskData[2];
        theDiskName.type="text";
        theDiskName.size=6;
      //  añadir tamaño de disco definiendo primeros 2 con minimo
        theDiskSize= document.createElement("input");
        theDiskSize.type="number";
        theDiskSize.required=true;
        theDiskSize.style="max-width:50px;";
        theDiskSize.name="tamo"+diskData[0]+"e"+diskData[1];
        theDiskSize.max=2048;
        theDiskSize.min=1;
        theDiskSize.value=diskData[3];
        theDiskSize.step=1;
      //  añadir descripcion de disco
        theDiskDescription = document.createElement("input");
        theDiskDescription.name="deso"+diskData[0]+"e"+diskData[1];
        theDiskDescription.value=diskData[4];
        theDiskDescription.type="text";
        theDiskDescription.size=6;
      /*  añadir cantidad de discos identicos
        TheDiskQuantity= document.createElement("input");
        TheDiskQuantity.type="number";
        TheDiskQuantity.required=true;
        TheDiskQuantity.style="max-width:35px;";
        TheDiskQuantity.name="QUo"+diskData[0]+"e"+diskData[1];
        TheDiskQuantity.max=20;
        TheDiskQuantity.min=1;
        TheDiskQuantity.value=diskData[5];*/
      //  agregar datos predefinidos a nueva tabla
        newTabla.appendChild(theDiskName);
        newTabla.appendChild(theDiskSize);
        newTabla.appendChild(theDiskDescription);
      /*  newTabla.appendChild(TheDiskQuantity);*/
        workInThisDisk.appendChild(newTabla);
    }
  </script>
  <body>
    <form method="post"  action="procesarMaquinas.php" class="containerOfRod">
      <br><br>
      <!-- Botones Principales -->
        <div class="shad" align="center">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit">
        </div>
        <br><br>
      <!-- Valores rescatados temp -->
        <input type="hidden" name="folio" value="<?php echo $_POST['folio'];?>" >
        <input type="hidden" name="action" value="<?php if($_POST['action']>2){$action1=$_POST['action']-2;}else {$action1=$_POST['action'];} echo $action1;?>" >
      <?php
        // Solicitud nueva
          if($_POST['action']==1)
          {
            //contar total de maquinas
              $totalMaquinas=$_POST['standalone'];
              $totalMaquinas += $_POST['apliance'];
              for($g=0;$g<$_POST['clusters'];$g++)
              {
                $temp="machinesOnCluster".$g;
                $totalMaquinas += $_POST[$temp];
              }
              echo "<input type=\"hidden\" name=\"totalMaquinas\" value=\"".$totalMaquinas."\" >";
            //preparacion de maquina por arreglo
              $machNumb=$actMaquinas[0]+1;
              $arregloActual=$arregloBG[0];
              $iniciaEn=0;
              $terminaEn=$_POST['standalone'];
              //maquinas Standalone
                for($contadorMaquina=0;$contadorMaquina<$_POST['standalone'];$contadorMaquina++)
                {
                  if($contadorMaquina==0)
                  {
                    $arregloActual++;
                    gimmetitles(0);
                  }
                  $someValues=array('arreglo' =>$arregloActual,'tipo'=>'Standalone','aplicacion'=>'','ambienteSolicitado'=>'','CPU'=>'','RAM'=>'','iniciaEn'=>$iniciaEn,'terminaEn'=>$terminaEn);
                  givesomemachine($someValues,$machNumb);
                  $machNumb++;
                }
                echo "</table><br><br>";
              $iniciaEn=$terminaEn;
              $terminaEn=$_POST['standalone']+$_POST['apliance'];
              //maquinas Apliance
                for($contadorMaquina=0;$contadorMaquina<$_POST['apliance'];$contadorMaquina++)
                {
                  if($contadorMaquina==0)
                  {
                    $arregloActual++;
                    gimmetitles(2);
                  }
                  $someValues=array('arreglo' =>$arregloActual,'tipo'=>'Apliance','aplicacion'=>'','ambienteSolicitado'=>'','CPU'=>'','RAM'=>'','iniciaEn'=>$iniciaEn,'terminaEn'=>$terminaEn);
                  givesomemachine($someValues,$machNumb);
                  $machNumb++;
                }
                echo "</table><br><br>";
              //Clusters
              for($contadorMaquina=0;$contadorMaquina<$_POST['clusters'];$contadorMaquina++)
              {
                $temp="machinesOnCluster".$contadorMaquina;
                gimmetitles(1);
                $arregloActual++;
                $iniciaEn=$terminaEn;
                $terminaEn=$terminaEn+$_POST[$temp];
                for($k=0;$k<$_POST[$temp];$k++)
                {
                  $someValues=array('arreglo' =>$arregloActual,'tipo'=>'Cluster','aplicacion'=>'','ambienteSolicitado'=>'','CPU'=>'','RAM'=>'','iniciaEn'=>$iniciaEn,'terminaEn'=>$terminaEn);
                  givesomemachine($someValues,$machNumb);
                  $machNumb++;
                }
                echo "</table><br>";
                echo "<br><br>";
              }
          }
        // Error de usuario
        if($_POST['action']==3)
        {
          echo '<script type="text/javascript">window.resizeTo(1280, 550);</script>';
          $arregloActual=0;
          $iniciaEn=0;
          $machineData= array('arreglo','tipo','aplicacion','ambienteSolicitado','CPU','RAM','staticDisk','sharedDisk');
          $staticData= array('name','tam','des');
          $sharedData= array('namo','tamo','deso');
          for($i=0;$i<$_POST['totalMaquinas'];$i++)
          {
            for($j=0;$j<sizeof($machineData);$j++)
              $maquinaData[$j]=$_POST[$machineData[$j].$i];
            if($maquinaData[0]!=$arregloActual)
            {
              if($arregloActual!=0)
                echo "</table><br><br>";
              if($maquinaData[1]=="Apliance")
                gimmetitles(2);
              else if($maquinaData[1]=="Cluster")
                gimmetitles(1);
              else
                gimmetitles(0);
              $arregloActual=$maquinaData[0];
              $iniciaEn=$i;
            }
            $someValues=array('arreglo' =>$maquinaData[0],'tipo'=>$maquinaData[1],'aplicacion'=>$maquinaData[2],'ambienteSolicitado'=>$maquinaData[3],'CPU'=>$maquinaData[4],'RAM'=>$maquinaData[5],'iniciaEn'=>$iniciaEn);
            recoverSomeMachine($someValues,$i);
            //chequeo de discos
            $StaticDisk=0;
            for($j=0;$j<$maquinaData[6];$j++)
            {
              for($h=0;$h<sizeof($staticData);$h++)
                $postedDisk[$h]=$staticData[$h].$j.'e'.$i;
              $diskValues=$StaticDisk.",".$i.",".$_POST[$postedDisk[0]].",".$_POST[$postedDisk[1]].",".$_POST[$postedDisk[2]];
              echo '<script type="text/javascript">addStatic("'.$diskValues.'")</script>';
              $StaticDisk++;
            }
            $SharedDisk=0;
            for($j=0;$j<$maquinaData[7];$j++)
            {
              for($h=0;$h<sizeof($sharedData);$h++)
                $postedDisk[$h]=$sharedData[$h].$j.'e'.$i;
              $diskValues=$SharedDisk.",".$i.",".$_POST[$postedDisk[0]].",".$_POST[$postedDisk[1]].",".$_POST[$postedDisk[2]];
              echo '<script type="text/javascript">addShared("'.$diskValues.'")</script>';
              $SharedDisk++;
            }
            if($maquinaData[6]>0)
            {
              $temp="staticDisk".$i;
              echo "<script type='text/javascript'>document.getElementById('$temp').disabled=false;</script>";
              echo "<script type='text/javascript'>document.getElementById('$temp').value=$StaticDisk;</script>";
            }
            if($maquinaData[7]>0||$maquinaData[1]=="Cluster")
            {
              $temp="sharedDisk".$i;
              echo "<script type='text/javascript'>document.getElementById('$temp').disabled=false;</script>";
              echo "<script type='text/javascript'>document.getElementById('$temp').value=$SharedDisk;</script>";
            }
          }
          echo "</table><br><br>";
          echo "<input type=\"hidden\" name=\"totalMaquinas\" value=\"".$_POST['totalMaquinas']."\" >";
        }
      ?>
      &ensp;&ensp;&ensp;&ensp;&ensp;
      <!--<button type="button" class="evilbtn">TD Automatización <br>Servicios Infraestructura</button>
      <br><br><br><br><br>-->
    </form>
    <?php
      function gimmetitles($titleType)
      {
        ?>
        <table align="center">
          <tr>
            <th  width="20">No:</th>
            <th  width="20">Aplicacion:</th>
            <th  width="20">Ambiente:</th>
            <th  width="20">vCPUs:</th>
            <th  width="20">Memoria RAM(gb):</th>
            <th  width="20">Disco Duro:</th>
            <th  width="45">
              <table>
                <tr>
                  <th  width="30%">Nombre</th>
                  <th  width="30%">Tamaño</th>
                  <th  width="30%">Descripcion</th>
                </tr>
              </table>
            </th>
            <?php if($titleType!=2)
              {
                if($titleType==1)
                {
                  ?>
                  <th  width="20">Disco Comp:</th>
                  <th  width="45">
                    <table>
                      <tr>
                        <th  width="30%">Nombre</th>
                        <th  width="30%">Tamaño</th>
                        <th  width="30%">Descripcion</th>
                      </tr>
                    </table>
                  </th>
                  <?php
                }
              }
            ?>
            <th  width="40">Especificaciones:</th>
          </tr>
        <?php
      }
      function givesomemachine($valuesOfMachine,$machineNumber)
      { 
        ?>
        <tr>
          <!-- Numero de maquina -->
            <td>
              <?php
                echo $machineNumber;
                include 'dbc.php';
                $conn = mysqli_connect($host,$user,$pass,$db);
              ?>
            </td>
          <!-- Numero de arreglo -->
            <input type="hidden" name="arreglo<?php echo $machineNumber;?>" value="<?php echo $valuesOfMachine['arreglo'];?>">
            <input type="hidden" name="tipo<?php echo $machineNumber;?>" value="<?php echo $valuesOfMachine['tipo'];?>" id="tipo<?php echo $machineNumber;?>">
          <!-- Aplicacion -->
            <td>
              <input type="text" size="4" required <?php if($valuesOfMachine['aplicacion']!="") {echo "value=\"".$valuesOfMachine['aplicacion']."\"";}?> name="aplicacion<?php echo $machineNumber;?>" autocomplete="off">
            </td>
          <!-- Ambiente Solicitado -->
            <td>
              <select name="ambienteSolicitado<?php echo $machineNumber;?>" required id="ambienteSolicitado<?php echo $machineNumber;?>" style="max-width:65px;">
                <option value=""></option>
                <?php
                  $sql="select nombre from ambiente";
                  $toFillTemplate = mysqli_query($conn,$sql);
                  if(! $toFillTemplate)
                    echo "<option value=\"\">Error de conexion</option>";
                  else
                    while($templateValues = mysqli_fetch_array($toFillTemplate))
                    {
                      if($templateValues['nombre']==$valuesOfMachine['ambienteSolicitado'])
                        $o ="<option value=\"".$templateValues['nombre']."\" selected>".$templateValues['nombre']."</option>";
                      else
                        $o ="<option value=\"".$templateValues['nombre']."\">".$templateValues['nombre']."</option>";
                      echo $o;
                      unset($o);
                    }
                ?>
              </select>
            </td>
          <!-- CPU -->
            <td>
              <input type="number" style="max-width:50px;" <?php if($valuesOfMachine['CPU']!="") {echo "value=\"".$valuesOfMachine['CPU']."\"";}?> required onkeypress="return isIntNumber(event)" name="CPU<?php echo $machineNumber;?>" id="CPU<?php echo $machineNumber;?>" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1" max="20">
            </td>
          <!-- RAM -->
            <td>
              <input type="number" style="max-width:50px;" <?php if($valuesOfMachine['RAM']!="") {echo "value=\"".$valuesOfMachine['RAM']."\"";}?> required onkeypress="return isIntNumber(event)" name="RAM<?php echo $machineNumber;?>" id="RAM<?php echo $machineNumber;?>" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1" max="255">
            </td>
          <!-- staticDisk Cantidad-->
            <td>
              <select id="staticDisk<?php echo $machineNumber;?>" name="staticDisk<?php echo $machineNumber;?>" onchange="giveStatic(this,<?php echo $machineNumber;?>)">
                <option value="0"></option>
                <?php
                  for($o=1;$o<10;$o++)
                    echo "<option value=\"".$o."\">".$o."</option>";
                ?>
              </select>
            </td>
          <!-- staticDisk Placeholder -->
            <td id="staticPlaceholder<?php echo $machineNumber;?>" style="max-width:260px;"> </td>
          <!-- Discos compartidos  -->
            <?php
              if($valuesOfMachine['tipo']=="Cluster")
              {
                ?>
                <td>
                  <select id="sharedDisk<?php echo $machineNumber;?>" name="sharedDisk<?php echo $machineNumber;?>"  onchange="giveShared(this,<?php echo $machineNumber;?>)">
                    <option value="0"></option>
                    <?php
                      for($o=1;$o<10;$o++)
                        echo "<option value=\"".$o."\">".$o."</option>";
                    ?>
                  </select>
                </td>
                <td id="sharedPlaceholder<?php echo $machineNumber;?>" style="max-width:260px;"></td>
                <?php
              }
            ?>
          <!-- Especificaciones -->
            <?php
              if($valuesOfMachine['tipo']=="Cluster"&&$machineNumber==$valuesOfMachine['iniciaEn'])
              {
                ?>
                <td>
                  <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $_POST['folio'].$machineNumber;?>&folio=<?php echo $_POST['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                  <br>
                  <input type="button" onclick="window.open('specs.php?accion=2&machine=<?php echo $_POST['folio'].$machineNumber;?>&folio=<?php echo $_POST['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="DB Requerimientos" />
                </td>
                <?php
              }
              else if($valuesOfMachine['tipo']!="Cluster")
              {
                ?>
                <td>
                  <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $_POST['folio'].$machineNumber;?>&folio=<?php echo $_POST['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                  <?php 
                    if($valuesOfMachine['tipo']!="Apliance")
                      echo "<br><input type=\"button\" onclick=\"window.open('specs.php?accion=2&machine=".$_POST['folio'].$machineNumber."&folio=".$_POST['folio']."','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')\" value=\"DB Requerimientos\" />";
                  ?>
                </td>
                <?php
              }
            ?>
        </tr>
        <?php
      }
      function recoverSomeMachine($valuesMachine,$machineNumb1)
      {
        ?> 
        <tr>
          <!-- Numero de maquina -->
            <td>
              <?php
                echo $machineNumb1+1;
                include 'dbc.php';
                $conn = mysqli_connect($host,$user,$pass,$db);
              ?>
            </td>
          <!-- Numero de arreglo -->
            <input type="hidden" name="arreglo<?php echo $machineNumb1;?>" value="<?php echo $valuesMachine['arreglo'];?>">
            <input type="hidden" name="tipo<?php echo $machineNumb1;?>" value="<?php echo $valuesMachine['tipo'];?>" id="tipo<?php echo $machineNumb1;?>">
          <!-- Aplicacion -->
            <td>
              <input type="text" size="4" required <?php if($valuesMachine['aplicacion']!="") {echo "value=\"".$valuesMachine['aplicacion']."\"";}?> name="aplicacion<?php echo $machineNumb1;?>" autocomplete="off">
            </td>
          <!-- Ambiente Solicitado -->
            <td>
              <select name="ambienteSolicitado<?php echo $machineNumb1;?>" required id="ambienteSolicitado<?php echo $machineNumb1;?>" style="max-width:65px;">
                <option value=""></option>
                <?php
                  $sql="select nombre from ambiente";
                  $toFillTemplate = mysqli_query($conn,$sql);
                  if(! $toFillTemplate)
                    echo "<option value=\"\">Error de conexion</option>";
                  else
                    while($templateValues = mysqli_fetch_array($toFillTemplate))
                    {
                      if($templateValues['nombre']==$valuesMachine['ambienteSolicitado'])
                        $o ="<option value=\"".$templateValues['nombre']."\" selected>".$templateValues['nombre']."</option>";
                      else
                        $o ="<option value=\"".$templateValues['nombre']."\">".$templateValues['nombre']."</option>";
                      echo $o;
                      unset($o);
                    }
                ?>
              </select>
            </td>
          <!-- CPU -->
            <td>
              <input type="number" style="max-width:50px;" <?php if($valuesMachine['CPU']!="") {echo "value=\"".$valuesMachine['CPU']."\"";}?> required onkeypress="return isIntNumber(event)" name="CPU<?php echo $machineNumb1;?>" id="CPU<?php echo $machineNumb1;?>" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1" max="20">
            </td>
          <!-- RAM -->
            <td>
              <input type="number" style="max-width:50px;" <?php if($valuesMachine['RAM']!="") {echo "value=\"".$valuesMachine['RAM']."\"";}?> required onkeypress="return isIntNumber(event)" name="RAM<?php echo $machineNumb1;?>" id="RAM<?php echo $machineNumb1;?>" autocomplete="off" maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" min="1" max="255">
            </td>
          <!-- staticDisk Cantidad-->
            <td>
              <select id="staticDisk<?php echo $machineNumb1;?>" name="staticDisk<?php echo $machineNumb1;?>" onchange="giveStatic(this,<?php echo $machineNumb1;?>)">
                <option value="0"></option>
                <?php
                  for($o=1;$o<10;$o++)
                    echo "<option value=\"".$o."\">".$o."</option>";
                ?>
              </select>
            </td>
          <!-- staticDisk Placeholder -->
            <td id="staticPlaceholder<?php echo $machineNumb1;?>" style="max-width:260px;"> </td>
          <!-- Discos compartidos  -->
            <?php
              if($valuesMachine['tipo']=="Cluster")
              {
                ?>
                <td>
                  <select id="sharedDisk<?php echo $machineNumb1;?>" name="sharedDisk<?php echo $machineNumb1;?>"  onchange="giveShared(this,<?php echo $machineNumb1;?>)">
                    <option value="0"></option>
                    <?php
                      for($o=1;$o<10;$o++)
                        echo "<option value=\"".$o."\">".$o."</option>";
                    ?>
                  </select>
                </td>
                <td id="sharedPlaceholder<?php echo $machineNumb1;?>" style="max-width:260px;"></td>
                <?php
              }
            ?>
          <!-- Especificaciones -->
            <?php
              if($valuesMachine['tipo']=="Cluster"&&$machineNumb1==$valuesMachine['iniciaEn'])
              {
                ?>
                <td>
                  <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $machineNumb1;?>&folio=<?php echo $_POST['solicitud'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                  <br>
                  <input type="button" onclick="window.open('specs.php?accion=2&machine=<?php echo $machineNumb1;?>&folio=<?php echo $_POST['solicitud'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="DB Requerimientos" />
                </td>
                <?php
              }
              else if($valuesMachine['tipo']!="Cluster")
              {
                ?>
                <td>
                  <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $machineNumb1;?>&folio=<?php echo $_POST['solicitud'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                  <?php 
                    if($valuesMachine['tipo']!="Apliance")
                      echo "<br><input type=\"button\" onclick=\"window.open('specs.php?accion=2&machine=".$machineNumb1."&folio=".$_POST['solicitud']."','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')\" value=\"DB Requerimientos\" />";
                  ?>
                </td>
                <?php
              }
            ?>
        </tr>
        <?php
      }
      mysqli_close($conn);
    ?>
  </body>
  <script>
    function isIntNumber(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
    function giveStatic(select,machineNumber)
    {
      //  recuperar y limpiar zona donde se trabajara
        workplace1="staticPlaceholder"+machineNumber;
        workplace1=document.getElementById(workplace1);
        while (workplace1.childNodes.length>select.value&&workplace1.lastChild!=workplace1.firstChild)
          workplace1.removeChild(workplace1.lastChild);
      //  Crear discos extra 
        if(workplace1.childNodes.length==0)
          some=0;
        else
          some=workplace1.childNodes.length-1;
        for(var creatorOfNodes=some;creatorOfNodes<select.value;creatorOfNodes++)
        {
          //  create nueva tabla de discos 
            newTabla = document.createElement("tr");
            newTabla.style="display:inline-block;";
          //  añadir nombre de disco
            theDiskName = document.createElement("input");
            theDiskName.name="name"+creatorOfNodes+"e"+machineNumber;
            theDiskName.value="DiskName"+(creatorOfNodes+1);
            theDiskName.type="text";
            theDiskName.size=6;
          //  añadir tamaño de disco
            theDiskSize= document.createElement("input");
            theDiskSize.type="number";
            theDiskSize.required=true;   
            theDiskSize.style="max-width:50px;";
            theDiskSize.name="tam"+creatorOfNodes+"e"+machineNumber;
            theDiskSize.max=2048;
            theDiskSize.min=1;
            theDiskSize.step=1;
          //  añadir descripcion de disco
            theDiskDescription = document.createElement("input");
            theDiskDescription.name="des"+creatorOfNodes+"e"+machineNumber;
            theDiskDescription.value="Descripcion";
            theDiskDescription.type="text";
            theDiskDescription.size=6;
          /*  añadir cantidad de discos identicos
            TheDiskQuantity= document.createElement("input");
            TheDiskQuantity.type="number";
            TheDiskQuantity.required=true;
            TheDiskQuantity.style="max-width:35px;";
            TheDiskQuantity.name="QU"+creatorOfNodes+"e"+machineNumber;
            TheDiskQuantity.max=100;
            TheDiskQuantity.min=1;
            TheDiskQuantity.value=1;*/
          //  agregar datos predefinidos a nueva tabla
            newTabla.appendChild(theDiskName);
            newTabla.appendChild(theDiskSize);
            newTabla.appendChild(theDiskDescription);
            /*newTabla.appendChild(TheDiskQuantity);*/
            workplace1.appendChild(newTabla);
        }
    }
    function giveShared(select,machineNumber1)
    {
      //  eliminar todos los discos
        workplace2="sharedPlaceholder"+machineNumber1;
        workplace2=document.getElementById(workplace2);
        while (workplace2.childNodes.length>select.value)
          workplace2.removeChild(workplace2.lastChild);
      //  crear nuevos discos
        for(var nodeShared=workplace2.childNodes.length;nodeShared<select.value;nodeShared++)
        {
          //  create nueva tabla de discos
            newTabla = document.createElement("tr");
            newTabla.style="display:inline-block;";
          //  añadir nombre de disco
            theDiskName = document.createElement("input");
            theDiskName.name="namo"+nodeShared+"e"+machineNumber1;
            theDiskName.value="Shared"+(nodeShared+1);
            theDiskName.type="text";
            theDiskName.size=6;
          //  añadir tamaño de disco
            theDiskSize= document.createElement("input");
            theDiskSize.type="number";
            theDiskSize.required=true;
            theDiskSize.style="max-width:50px;";
            theDiskSize.name="tamo"+nodeShared+"e"+machineNumber1;
            theDiskSize.max=2048;
            theDiskSize.min=1;
            theDiskSize.step=1;
          //  añadir descripcion de disco
            theDiskDescription = document.createElement("input");
            theDiskDescription.name="deso"+nodeShared+"e"+machineNumber1;
            theDiskDescription.value="Descripcion";
            theDiskDescription.type="text";
            theDiskDescription.size=6;
          /*  añadir cantidad de discos identicos
            TheDiskQuantity= document.createElement("input");
            TheDiskQuantity.type="number";
            TheDiskQuantity.required=true;
            TheDiskQuantity.style="max-width:35px;";
            TheDiskQuantity.name="QUo"+nodeShared+"e"+machineNumber1;
            TheDiskQuantity.max=100;
            TheDiskQuantity.min=1;
            TheDiskQuantity.value=1;*/
          //  agregar datos predefinidos a nueva tabla
            newTabla.appendChild(theDiskName);
            newTabla.appendChild(theDiskSize);
            newTabla.appendChild(theDiskDescription);
            /*newTabla.appendChild(TheDiskQuantity);*/
            workplace2.appendChild(newTabla);
        }
    }
  </script>
</html>