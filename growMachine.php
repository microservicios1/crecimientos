<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="StRod.css">
  <title>Maquinas Virtuales</title>
  <?php
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
  ?>
  <style>
    table
    {
      border: 1px solid #000000;
      width: 100%;
      text-align: center;
      border-collapse: collapse;
    }
    td,th 
    {
      border: 1px solid #000000;
      padding: 3px 2px;
    }
    .containerOfRod
    {
      padding: 4px 4px;
      box-sizing: border-box;
      font-size: 14px;
      border:10px groove #616161;
      border-radius: 10px;
    }
  </style>
  <script>
    function isip(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
        return false;
      return true;
    }
    function isdate(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=45))
        return false;
      return true;
    }
    function isSolid(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
  </script>
</head>
<script>
  function addStatic(diskInfo)
  {
    //  recibir los datos
      var diskData = diskInfo.split(",");
    //  recuperar en que disco se trabajara
      solicitadoStatic="staticSolicitado"+diskData[1];
      solicitadoStatic=document.getElementById(solicitadoStatic);
    //  create nueva tabla de discos
      entregaTabla = document.createElement("tr");
    //  añadir nombre de disco  solicitado
      theUsernamePlace = document.createElement("td");
      theUsernamePlace.width="33%";
      theUsernamePlace.style="border:none;";
      entregaDiskName = document.createElement("input");
      entregaDiskName.name="name"+diskData[0]+"s"+diskData[1];
      entregaDiskName.value=diskData[2];
      entregaDiskName.type="text";
      entregaDiskName.size=5;
      theUsernamePlace.appendChild(entregaDiskName);
    //  añadir tamaño de disco solicitado
      entregaDiskSizePlace = document.createElement("td");
      entregaDiskSizePlace.width="30%";
      entregaDiskSizePlace.style="border:none;";
      entregaDiskSize= document.createElement("input");
      entregaDiskSize.type="number";
      entregaDiskSize.style="max-width:50px;";
      entregaDiskSize.name="tam"+diskData[0]+"s"+diskData[1];
      entregaDiskSize.max=2048;
      entregaDiskSize.min=0;
      entregaDiskSize.value=diskData[3];
      entregaDiskSize.step=1;
      entregaDiskSizePlace.appendChild(entregaDiskSize);
    //  añadir notas de disco solicitado
      entregaDiskNotasPlace = document.createElement("td");
      entregaDiskNotasPlace.width="33%";
      entregaDiskNotasPlace.style="border:none;";
      entregaDiskNotas = document.createElement("input");
      entregaDiskNotas.name="not"+diskData[0]+"e"+diskData[1];
      entregaDiskNotas.value=diskData[4];
      entregaDiskNotas.type="text";
      entregaDiskNotas.size=5;
      entregaDiskNotasPlace.appendChild(entregaDiskNotas);
    /*solicitaTabla.appendChild(solicitaDiskQuantity);*/
      entregaTabla.appendChild(theUsernamePlace);
      entregaTabla.appendChild(entregaDiskSizePlace);
      entregaTabla.appendChild(entregaDiskNotasPlace);
    /*entregaTabla.appendChild(entregaDiskQuantity);*/
      solicitadoStatic.appendChild(entregaTabla);
  }
  function addShared(diskInfo)
  {
    //  recibir los datos
      var diskData = diskInfo.split(",");
    //  recuperar en que disco se trabajara
      solicitadoShared="sharedSolicitado"+diskData[1];
      solicitadoShared=document.getElementById(solicitadoShared);
    //  create nueva tabla de discos
      solicitaTabla = document.createElement("tr");
      solicitaTabla.style="display:inline-block;";
    //  añadir nombre de disco  solicitado
      solicitaDiskName = document.createElement("input");
      solicitaDiskName.name="namo"+diskData[0]+"s"+diskData[1];
      solicitaDiskName.value=diskData[2];
      solicitaDiskName.type="text";
      solicitaDiskName.size=4;
    //  añadir tamaño de disco solicitado
      solicitaDiskSize= document.createElement("input");
      solicitaDiskSize.type="number";
      solicitaDiskSize.style="max-width:50px;";
      solicitaDiskSize.name="tamo"+diskData[0]+"s"+diskData[1];
      solicitaDiskSize.max=2048;
      solicitaDiskSize.min=0;
      solicitaDiskSize.value=diskData[4];
      solicitaDiskSize.step=1;
    //  añadir descripcion de disco solicitado
      solicitaDiskDescription = document.createElement("input");
      solicitaDiskDescription.name="deso"+diskData[0]+"s"+diskData[1];
      solicitaDiskDescription.value=diskData[6];
      solicitaDiskDescription.type="text";
      solicitaDiskDescription.size=4;
    //  agregar datos predefinidos a nueva tabla
      solicitaTabla.appendChild(solicitaDiskName);
      solicitaTabla.appendChild(solicitaDiskSize);
      solicitaTabla.appendChild(solicitaDiskDescription);
      solicitadoShared.appendChild(solicitaTabla);
  }
</script>
<body>
  <form method="post"  action="solCrecimiento.php" class="containerOfRod">
    <br><br>
    <!-- Botones Principales -->
      <div class="shad" align="center">
        <?php for($i=0;$i<8;$i++) echo "&nbsp;"; ?><input type="submit" value="Solicitar cambio"> &nbsp;
      </div>
      <br><br>
      Vacie un disco para solicitar su eliminación.
    <!-- Valores rescatados temp -->
      <input type="hidden" name="folio" value="<?php echo $_POST['folio'];?>" >
    <?php
      $sql="select arreglo from maquinas where folio='".$_POST['folio']."' group by arreglo";
      $grupos = mysqli_query($conn,$sql);
      $howManyMachines=mysqli_affected_rows($conn);
      if($howManyMachines<1)
        echo "<h2 align=\"center\">Ninguna maquina registrada en esta solicitud.".$_POST['folio']."</h2>";
      else
      {
        $Maquina=0;
        $Maquina2=0;
        $iniciaEn=0;
        $terminaEn=0;
        while($arregloRecuperado = mysqli_fetch_array($grupos))
        {
          $sql="select folio,interId,tipo,aplicacion,ambienteSolicitado,detalleEstatus,TDOYM,estatusOYM,ambienteEntregado,CPUSolicitado,RAMSolicitado,CPUEntregado,RAMEntregado,nombre,ip,infraestructuraDef,estatus from maquinas where folio='".$_POST['folio']."' and arreglo=".$arregloRecuperado['arreglo'];
          $consultaMaquinas = mysqli_query($conn,$sql);
          $trigger1=0;
          while($maquinasRecuperadas = mysqli_fetch_array($consultaMaquinas))
          {
            //      determina tamaño y titulos
              if($trigger1==0)
              {
                $iniciaEn=$terminaEn;
                $sql="select count(arreglo) from maquinas where folio='".$_POST['folio']."' and arreglo=".$arregloRecuperado['arreglo'];
                $realSize = mysqli_fetch_array(mysqli_query($conn,$sql));
                $terminaEn += $realSize[0];
                echo "<br>".$maquinasRecuperadas['tipo'].":<br>";
                gimmetitles();
                $trigger1=1;
              }
            $someValues=array('folio' => $maquinasRecuperadas['folio'],'interId' => $maquinasRecuperadas['interId'],'tipo' => $maquinasRecuperadas['tipo'],'aplicacion' => $maquinasRecuperadas['aplicacion'],'detalleEstatus' => $maquinasRecuperadas['detalleEstatus'],'estatusOYM' => $maquinasRecuperadas['estatusOYM'],'ambienteEntregado' => $maquinasRecuperadas['ambienteEntregado'],'TDOYM'=>$maquinasRecuperadas['TDOYM'],'ambienteSolicitado' => $maquinasRecuperadas['ambienteSolicitado'],'CPUSolicitado'=>$maquinasRecuperadas['CPUSolicitado'],'RAMSolicitado'=>$maquinasRecuperadas['RAMSolicitado'],'CPUEntregado'=>$maquinasRecuperadas['CPUEntregado'],'RAMEntregado'=>$maquinasRecuperadas['RAMEntregado'],'nombre'=>$maquinasRecuperadas['nombre'],'ip'=>$maquinasRecuperadas['ip'],'infraestructuraDef'=>$maquinasRecuperadas['infraestructuraDef'],'estatus'=>$maquinasRecuperadas['estatus'],'iniciaEn'=>$iniciaEn,'terminaEn'=>$terminaEn);
            givesomemachine($conn,$someValues,$Maquina);
            $Maquina++;
          }
          echo "</table><br>";
          $sql="select interId,tipo,ip,entregaUser,remedy,fechaSoliMOP,fechaEntregaServer from maquinas where folio='".$_POST['folio']."' and arreglo=".$arregloRecuperado['arreglo'];
          $consultaMaquinas = mysqli_query($conn,$sql);
          while($maquinasRecuperadas = mysqli_fetch_array($consultaMaquinas))
          {
            //      determina tamaño y titulos
              if($trigger1==1)
              {
                secTable($maquinasRecuperadas['tipo']);
                $trigger1=2;
              }
            $someValues2=array('interId' =>$maquinasRecuperadas['interId'],'tipo' => $maquinasRecuperadas['tipo'],'ip'=>$maquinasRecuperadas['ip'],'entregaUser' => $maquinasRecuperadas['entregaUser'],'remedy'=>$maquinasRecuperadas['remedy'],'fechaSoliMOP'=>$maquinasRecuperadas['fechaSoliMOP'],'fechaEntregaServer'=>$maquinasRecuperadas['fechaEntregaServer'],'iniciaEn'=>$iniciaEn,'terminaEn'=>$terminaEn);
            givesecmachine($conn,$someValues2,$Maquina2);
            //      discos
              $sql="select nombreDiscoSolicitado,sizeDiscoSolicitado,tipoDisco,descripcion from discos where interId='".$maquinasRecuperadas['interId']."'";
              $resultDisk = mysqli_query($conn,$sql);
              $disk=0;
              while($rescuedDisk = mysqli_fetch_array($resultDisk))
              {
                $diskValues=$disk.",".$Maquina2.",".$rescuedDisk['nombreDiscoSolicitado'].",".$rescuedDisk['sizeDiscoSolicitado'].",".$rescuedDisk['descripcion'];
                if($rescuedDisk['tipoDisco']=="Estatico")
                  echo '<script type="text/javascript">addStatic("'.$diskValues.'")</script>';
                else
                  echo '<script type="text/javascript">addShared("'.$diskValues.'")</script>';
                $disk++;
               }
            $Maquina2++;
          }
          echo "</table><br>";
        }
        echo "<input type=\"hidden\" name=\"totalMaquinas\" value=\"".$Maquina."\" >";
      }
      function gimmetitles()
      {
        ?>
        <table align="center">
          <tr>
            <th width="20">IP:</th>
            <th width="20">Nombre:</th>
            <th width="20">Estatus:</th>
            <th width="20">Detalle <br />de estatus:</th>
            <th width="20">Infraestructura<br>definida:</th>
            <th width="20">Aplicacion</th>
            <th width="20">Ambiente</th>
            <th width="20">vCPU:</th>
            <th width="20">RAM:</th>
            <th width="20">TD/OYM:</th>
            <th width="20">Especificaciones:</th>
          </tr>
        <?php
      }
      function secTable($tipo)
      {
        ?>
        </table><br>
        <table align="center">
          <tr>
            <th>IP:</th>
            <th>
              <table>
                <tr>
                  <th colspan="3">Disco Actual</th>
                </tr>
                <tr>
                  <td>Nombre</td>
                  <td>Tamaño</td>
                  <td>Notas</td>
                </tr>
              </table>
            </th>
            <th>Disco Duro:</th>
            <th>
              <table>
                <tr>
                  <th colspan="3">Disco solicitado</th>
                </tr>
                <tr>
                  <td>Nombre</td>
                  <td>Tamaño</td>
                  <td>Notas</td>
                </tr>
              </table>
            </th>
            <?php
              if($tipo=="Cluster")
              {
                ?>
                <th>
                  <table>
                    <tr>
                      <th  colspan="3">Disco Compartido Actual</th>
                    </tr>
                    <tr>
                      <td>Nombre</td>
                      <td>Tamaño</td>
                      <td>Descripcion</td>
                    </tr>
                  </table>
                </th>
                <th>Disco Compartido:</th>
                <th>
                  <table>
                    <tr>
                      <th  colspan="3">Disco Compartido Solicitado</th>
                    </tr>
                    <tr>
                      <td>Nombre</td>
                      <td>Tamaño</td>
                      <td>Notas</td>
                    </tr>
                  </table>
                </th>
                <?php
              }
            ?>
            <th>F. Solicitud <br />de MOP:</th>
            <th>F. Entrega <br />server:</th>
            <th>F. Entrega <br />a User:</th>
          </tr>
        <?php
      }
      function givesomemachine($conn,$infoData,$maquina)
      {
        ?>
        <tr>
          <td><input type="text" size="8" name="ip<?php echo $maquina;?>" readonly="readonly" value=<?php echo "\"".$infoData['ip']."\""; if($infoData['ip']=="") echo " style=\"background:#85807d\"";?> onkeypress="return isip(event)" maxlength="16" oninput="if(this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"></td>
          <td><input type="text" size="8" name="nombre<?php echo $maquina;?>" value=<?php echo "\"".$infoData['nombre']."\""; if($infoData['nombre']=="") echo " style=\"background:#85807d\"";?> autocomplete="off" ></td>
          <td>
            <select name="estatus<?php echo $maquina;?>" style="width:100px;<?php if($infoData['estatus']=="") echo "background:#85807d";?>" autocomplete="off">
              <option value="" selected></option>
              <option value="EN PROCESO" <?php if($infoData['estatus'] == "EN PROCESO") echo " selected ";?> >EN PROCESO</option>
              <option value="PENDIENTE" <?php if($infoData['estatus'] == "PENDIENTE") echo " selected ";?> >PENDIENTE</option>
              <option value="ENTREGADO" <?php if($infoData['estatus'] == "ENTREGADO") echo " selected ";?> >ENTREGADO</option>
              <option value="CANCELADO" <?php if($infoData['estatus'] == "CANCELADO") echo " selected ";?> >CANCELADO</option>
            </select>
          </td>
          <td><input type="text" size="8" name="detalleEstatus<?php echo $maquina;?>" style="width:60px;<?php if($infoData['detalleEstatus']=="") echo "background:#85807d;";?>" autocomplete="off" value="<?php echo $infoData['detalleEstatus'];?>" ></td>
          <input type="hidden" name=<?php echo "\"interId".$maquina."\"";  echo "value=\"".$infoData['interId']."\"";?>>
          <input type="hidden" name=<?php echo "\"tipo".$maquina."\"";  echo "value=\"".$infoData['tipo']."\"";?>>
          <td>
            <select name="infraestructuraDef<?php echo $maquina;?>" style="width:100px;<?php if($infoData['infraestructuraDef']=="") echo "background:#85807d;";?>" autocomplete="off">
              <option value=""></option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from infdef");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($infoData['infraestructuraDef'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
          </td>
          <td><input type="text" size="10" name="aplicacion<?php echo $maquina;?>" value=<?php echo "\"".$infoData['aplicacion']."\""; if($infoData['aplicacion']=="") echo " style=\"background:#85807d\"";?> autocomplete="off" ></td>
          <td>
            <select name="ambiente<?php echo $maquina;?>" style="width:100px" autocomplete="off">
              <option value=""></option>
              <?php
                $re4 = mysqli_query($conn,"select nombre from ambiente");
                if(! $re4)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re4))
                  {
                    $o ="<option ";
                    if($infoData['ambienteEntregado'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
          </td>
          <td><input type="number" style="max-width:80px;" min="1" max="20" name=<?php echo "\"CPU".$maquina."\""; if($infoData['CPUSolicitado']==""||$infoData['CPUSolicitado']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" <?php echo "value=\"".$infoData['CPUEntregado']."\""; ?>></td>
          <td><input type="number" style="max-width:80px;" min="1" max="255" name=<?php echo "\"RAM".$maquina."\""; if($infoData['RAMSolicitado']==""||$infoData['RAMSolicitado']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" <?php echo "value=\"".$infoData['RAMEntregado']."\""; ?>></td>
          <td>
            <select name="TDOYM<?php echo $maquina;?>" style="width:100px;<?php if($infoData['TDOYM']=="") echo "background:#85807d";?>" autocomplete="off">
              <option value=""></option>
              <option value="1" <?php if($infoData['TDOYM'] == "1") echo " selected ";?> >TD</option>
              <option value="2" <?php if($infoData['TDOYM'] == "2") echo " selected ";?> >OYM</option>
            </select>
          </td>
          <?php
            if($infoData['tipo']=="Cluster"&&$maquina==$infoData['iniciaEn'])
            {
              ?>
              <td>
                <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $infoData['interId'];?>&folio=<?php echo $infoData['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                <br>
                <input type="button" onclick="window.open('specs.php?accion=2&machine=<?php echo $infoData['interId'];?>&folio=<?php echo $infoData['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="DB Requerimientos" />
              </td>
              <?php
            }
            else if($infoData['tipo']!="Cluster")
            {
              ?>
              <td>
                <input type="button" onclick="window.open('specs.php?accion=1&machine=<?php echo $infoData['interId'];?>&folio=<?php echo $infoData['folio'];?>','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')" value="S.O.Requerimientos" />
                <?php 
                  if($infoData['tipo']!="Apliance")
                    echo "<br><input type=\"button\" onclick=\"window.open('specs.php?accion=2&machine=".$infoData['interId']."&folio=".$infoData['folio']."','','menubar=0,titlebar=0,width=1000,height=600,resizable=0,left=40px,top=50px')\" value=\"DB Requerimientos\" />";
                ?>
              </td>
              <?php
            }
          ?>
        </tr>
        <?php
      }
      function givesecmachine($conn,$infoData2,$maquina2)
      {
        ?>
        <tr>
          <td><input type="text" size="8" value=<?php echo "\"".$infoData2['ip']."\""; if($infoData2['ip']=="") echo " style=\"background:#85807d\"";?> readonly="readonly"></td>
          <td id="staticSolicitado<?php echo $maquina2;?>" style="max-width:260px;"> </td>
          <td>
            <select id="staticDisk<?php echo $maquina2;?>" name="staticDisk<?php echo $maquina2;?>" onchange="giveStatic(this,<?php echo $maquina2;?>)">
              <option value="0"></option>
              <?php
                for($o=1;$o<10;$o++)
                  echo "<option value=\"".$o."\">".$o."</option>";
              ?>
            </select>
          </td>
          <td id="nuevoStatic<?php echo $maquina2;?>" style="max-width:260px;"> </td>
          <?php
            if($infoData2['tipo']=="Cluster")
            {
              ?>
              <td id="sharedSolicitado<?php echo $maquina2;?>" style="max-width:260px;"></td>
              <td>
                <select id="sharedDisk<?php echo $maquina2;?>" name="sharedDisk<?php echo $maquina2;?>" onchange="giveShared(this,<?php echo $maquina2;?>)">
                  <option value="0"></option>
                  <?php
                    for($o=1;$o<10;$o++)
                      echo "<option value=\"".$o."\">".$o."</option>";
                  ?>
                </select>
              </td>
              <td id="nuevoShared<?php echo $maquina2;?>" style="max-width:260px;"></td>
              <?php
            }
          ?>
          <td><input type="date" readonly="readonly" name=<?php echo "\"fechaSoliMOP".$maquina2."\" value=\"".$infoData2['fechaSoliMOP']."\" style=\"width:130px;";if($infoData2['fechaSoliMOP']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
          <td><input type="date" readonly="readonly" name=<?php echo "\"fechaEntregaServer".$maquina2."\" value=\"".$infoData2['fechaEntregaServer']."\" style=\"width:130px;";if($infoData2['fechaEntregaServer']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
          <td><input type="date" readonly="readonly" name=<?php echo "\"entregaUser".$maquina2."\" value=\"".$infoData2['entregaUser']."\" style=\"width:130px;";if($infoData2['entregaUser']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
        </tr>
        <?php
      }
    ?>
  </form>
</body>
<script>
  function giveStatic(select,machineNumber)
  {
    //  recuperar y limpiar zona donde se trabajara
      workplace1="nuevoStatic"+machineNumber;
      workplace1=document.getElementById(workplace1);
      while (workplace1.childNodes.length>select.value&&workplace1.lastChild!=workplace1.firstChild)
        workplace1.removeChild(workplace1.lastChild);
    //  Crear discos extra 
      if(workplace1.childNodes.length==0)
        some=0;
      else
        some=workplace1.childNodes.length-1;
      for(var creatorOfNodes=some;creatorOfNodes<select.value;creatorOfNodes++)
      {
        //  create nueva tabla de discos 
          newTabla = document.createElement("tr");
        //  añadir nombre de disco
          theDiskNamePlace = document.createElement("td");
          theDiskNamePlace.width="33%";
          theDiskNamePlace.style="border:none;";
          theDiskName = document.createElement("input");
          theDiskName.name="name"+creatorOfNodes+"e"+machineNumber;
          theDiskName.value="DiskName"+(creatorOfNodes+1);
          theDiskName.type="text";
          theDiskName.size=6;
          theDiskNamePlace.appendChild(theDiskName);
        //  añadir tamaño de disco
          theDiskSizePlace = document.createElement("td");
          theDiskSizePlace.width="33%";
          theDiskSizePlace.style="border:none;";
          theDiskSize= document.createElement("input");
          theDiskSize.type="number";
          theDiskSize.required=true;   
          theDiskSize.style="max-width:50px;";
          theDiskSize.name="tam"+creatorOfNodes+"e"+machineNumber;
          theDiskSize.max=2048;
          theDiskSize.min=1;
          theDiskSize.step=1;
          theDiskSizePlace.appendChild(theDiskSize);
        //  añadir descripcion de disco
          theDiskDescriptionPlace = document.createElement("td");
          theDiskDescriptionPlace.width="33%";
          theDiskDescriptionPlace.style="border:none;";
          theDiskDescription = document.createElement("input");
          theDiskDescription.name="des"+creatorOfNodes+"e"+machineNumber;
          theDiskDescription.value="Descripcion";
          theDiskDescription.type="text";
          theDiskDescription.size=6;
          theDiskDescriptionPlace.appendChild(theDiskDescription);
        /*solicitaTabla.appendChild(solicitaDiskQuantity);*/
          newTabla.appendChild(theDiskNamePlace);
          newTabla.appendChild(theDiskSizePlace);
          newTabla.appendChild(theDiskDescriptionPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
          workplace1.appendChild(newTabla);
      }
  }
  function giveShared(select,machineNumber1)
  {
    //  eliminar todos los discos
      workplace2="nuevoShared"+machineNumber1;
      workplace2=document.getElementById(workplace2);
      while (workplace2.childNodes.length>select.value)
        workplace2.removeChild(workplace2.lastChild);
    //  crear nuevos discos
      for(var nodeShared=workplace2.childNodes.length;nodeShared<select.value;nodeShared++)
      {
        //  create nueva tabla de discos
          newTabla = document.createElement("tr");
        //  añadir nombre de disco
          theDiskNamePlace = document.createElement("td");
          theDiskNamePlace.width="33%";
          theDiskNamePlace.style="border:none;";
          theDiskName = document.createElement("input");
          theDiskName.name="namo"+nodeShared+"e"+machineNumber1;
          theDiskName.value="Shared"+(nodeShared+1);
          theDiskName.type="text";
          theDiskName.size=6;
          theDiskNamePlace.appendChild(theDiskName);
        //  añadir tamaño de disco
          theDiskSizePlace = document.createElement("td");
          theDiskSizePlace.width="33%";
          theDiskSizePlace.style="border:none;";
          theDiskSize= document.createElement("input");
          theDiskSize.type="number";
          theDiskSize.required=true;
          theDiskSize.style="max-width:50px;";
          theDiskSize.name="tamo"+nodeShared+"e"+machineNumber1;
          theDiskSize.max=2048;
          theDiskSize.min=1;
          theDiskSize.step=1;
          theDiskSizePlace.appendChild(theDiskSize);
        //  añadir descripcion de disco
          theDiskDescriptionPlace = document.createElement("td");
          theDiskDescriptionPlace.width="33%";
          theDiskDescriptionPlace.style="border:none;";
          theDiskDescription = document.createElement("input");
          theDiskDescription.name="deso"+nodeShared+"e"+machineNumber1;
          theDiskDescription.value="Descripcion";
          theDiskDescription.type="text";
          theDiskDescription.size=6;
          theDiskDescriptionPlace.appendChild(theDiskDescription);
        //  agregar datos predefinidos a nueva tabla
          newTabla.appendChild(theDiskNamePlace);
          newTabla.appendChild(theDiskSizePlace);
          newTabla.appendChild(theDiskDescriptionPlace);
        /*newTabla.appendChild(TheDiskQuantity);*/
          workplace2.appendChild(newTabla);
      }
  }
</script>
</html>