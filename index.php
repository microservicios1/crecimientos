<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Crecimientos</title>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <?php
      include 'dbc.php';
      include 'session.php';
    ?>
    <style>
      th
      {
        font-size: 16px;
        border: 1px solid black;
        text-align: center;
      }
      td
      {
        text-align: center;
        font-size: 16px;
        border: 1px solid black;
      }
    </style>
  </head>
  <body>
    <div class="container" align="center">
    <ul id="nav">
        <li><a href="<?php echo $logout;?>">Cerrar sesion</a></li>
        <li>Hola : <?php echo $_COOKIE['userName'];?></li>
        <li><a href="<?php echo $crecimientos;?>">Crecimientos</a></li>
        <?php
          if($_COOKIE['userLvl']==1)
          {
            if($_COOKIE['userName']=='VY8G08A')
            {
              ?>
              <li><a href="<?php echo $consulk;?>">Spec Ops</a></li>
              <?php
            }
            ?>
            <li><a href="<?php echo $solicitudes;?>">Crear Solicitud</a></li>
            <li><a href="<?php echo $reporte;?>">Reportes</a></li>
            <li><a href="<?php echo $choose;?>">Solicitudes Actuales</a></li>
            <li><a href="<?php echo $inside;?>">Proyectos</a></li>
            <?php
          }
          else
          {
            ?>
            <li class="current"><a href="<?php echo $index;?>">Solicitudes</a></li>
            <?php
          }
        ?>
      </ul>
      <br><br>
      <table width="60%">
        <tr>
          <h2> Seleccionar proyecto en el que se desea el crecimiento: </h2>
        </tr>
        <?php
          $conn = mysqli_connect($host, $user, $pass, $db);   
          $sql="select folio,fecha,proyecto from proyectos where solicita='".$_COOKIE['userName']."'";
          $re = mysqli_query($conn,$sql);
          $r=mysqli_affected_rows($conn);
          if($r<1)
            echo "<tr><td>Ningun proyecto ha sido registrado bajo su usuario</tr></td>";
          else
            while($row = mysqli_fetch_array($re))
              echo "<tr><td><br><a href='cc.php?folio=".$row['folio']."'>Proyecto: ".$row['proyecto'].",solicitado el ".$row['fecha']." </a></tr></td>";
          mysqli_close($conn);
        ?>
      </table>  
      <br>
    </div>
  </body>
</html>